from django.shortcuts import render, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from .forms import ProjectForm



# Create your views here.


# List of Projects
@login_required
def project_list(request):
    project = Project.objects.filter(owner=request.user)
    context = {
        "project_list": project,
    }
    return render(request, "projects/project_list.html", context)


# Create new project
@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)


# Detail views
@login_required
def show_project_details(request, id):
    project_details = Project.objects.get(id=id)
    context = {
        "project_details": project_details,
    }
    return render(request, "projects/project_details.html", context)
